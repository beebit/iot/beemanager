'use strict';

module.exports.hello = async (event, context) => {
  if(!event.hasOwnProperty('public_key') || !event.hasOwnProperty('mac_address') || !event.hasOwnProperty('signed_timestamp')){
    return {
      statusCode: 500,
      body: "Format error"
    }
  }

  let mac_address_regex = /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/;
  if(!mac_address_regex.test(event.mac_address)){
      return {
          statusCode: 500,
          body: "Format error, invalid MAC"
      }
  }

  return {
    statusCode: 200
  };
};
