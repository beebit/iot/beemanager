'use strict';
const {NodeRSA} = require('node-rsa');
const {withProcessEnv} = require('./utils/database');
const {DeviceRegistry} = require('./models/device');
const {atob} = require('atob');
const uuid = require('uuid/v4');

module.exports.hello = async (event, context) => {
    if (!event.hasOwnProperty('public_key') || !event.hasOwnProperty('mac_address') || !event.hasOwnProperty('signed_timestamp')) {
        return {
            statusCode: 500,
            body: "Format error"
        }
    }

    let key = new NodeRSA();
    let token = context.signed_timestamp.split('.');
    token = {
        timestamp:token[0],
        signature:token[1]
    };
    key.importKey(context.public_key,'pkcs1-public');
    if(!key.verify(Buffer.from(token.timestamp).toString('base64'),token.signature,'base64','base64')){
        return {
            statusCode: 500,
            body: "Signed timestamp error";
        }
    }

    let mac_address = event.mac_address;
    mac_address = (mac_address.toUpperCase()).replace('-', ':');
    let mac_address_regex = /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/;
    if (!mac_address_regex.test(mac_address)) {
        return {
            statusCode: 500,
            body: "Format error, invalid MAC"
        }
    }

    let timestamp = atob(token.timestamp);

    let device = DeviceRegistry.put({
        id:uuid(),
        mac_address:mac_address,
        public_key:context.public_key,
        created_at:timestamp
    });

    return {
        device: device,
        statusCode: 200
    };
};
